# -*- coding: utf-8 -*
import os
import time
# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text
def test_q1():
	os.system('adb shell uiautomator dump sdcard/q1.xml && adb pull /sdcard/q1.xml .')
	f = open('q1.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('查看商品分類') != -1
# 2. [Screenshot] Side Bar Text
def test_q2():
	os.system('adb shell screencap -p /sdcard/q2.png')
	os.system('adb pull /sdcard/q2.png')
	os.system('adb shell input tap 958 707')
# 3. [Context] Categories
def test_q3():
	os.system('adb shell input swipe 743 1352 743 1142')
	time.sleep(1)
	os.system('adb shell input tap 999 1440')
	time.sleep(1)
	os.system('adb shell uiautomator dump sdcard/q3.xml && adb pull /sdcard/q3.xml .')
	f = open('q3.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('精選') != -1		
	#os.system('adb shell input  958 707')	
# 4. [Screenshot] Categories
def test_q4():
	os.system('adb shell screencap -p /sdcard/q4.png')
	os.system('adb pull /sdcard/q4.png')
# 5. [Context] Categories page
def test_q5():
	os.system('adb shell input tap 299 1728')
	time.sleep(1)	
	os.system('adb shell input tap 299 1728')
	time.sleep(1)
	os.system('adb shell uiautomator dump sdcard/q5.xml && adb pull /sdcard/q5.xml .')
	f = open('q5.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('運動戶外') != -1	
# 6. [Screenshot] Categories page
def test_q6():
	os.system('adb shell screencap -p /sdcard/q6.png')
	os.system('adb pull /sdcard/q6.png')
# 7. [Behavior] Search item “switch”
def test_q7():
	os.system('adb shell input tap 419 148')
	time.sleep(1)
	os.system('adb shell input text "switch"')
	os.system('adb shell input keyevent "KEYCODE_ENTER"')
	time.sleep(1)	

# 8. [Behavior] Follow an item and it should be add to the list
def test_q8():
	os.system('adb shell input tap 590 597')
	time.sleep(1)	
	os.system('adb shell input tap 88 1716')
	time.sleep(1)	
	os.system('adb shell input tap 88 121')
	time.sleep(1)	
	os.system('adb shell input tap 83 1733')
	time.sleep(1)	
	os.system('adb shell input tap 100 100')
	time.sleep(1)	
	os.system('adb shell input tap 334 890')
	time.sleep(1)	
	os.system('adb shell input swipe 607 770 607 1220')
	time.sleep(3)
	os.system('adb shell uiautomator dump sdcard/q8.xml && adb pull /sdcard/q8.xml .')
	f = open('q8.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('Switch') != -1		
	




# 9. [Behavior] Navigate to the detail of item
def test_q9():
	os.system('adb shell input tap 281 788')	
	time.sleep(3)	
	os.system('adb shell input tap 494 126')	
	time.sleep(3)
	os.system('adb shell uiautomator dump sdcard/q9.xml && adb pull /sdcard/q9.xml .')
	f = open('q9.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('商品特色') != -1		

	

# 10. [Screenshot] Disconnetion Screen
def test_q10():
	os.system('adb shell svc wifi disable')	
	os.system('adb shell svc data disable')	
	time.sleep(1)	
	os.system('adb shell screencap -p /sdcard/q10.png')
	os.system('adb pull /sdcard/q10.png')	
